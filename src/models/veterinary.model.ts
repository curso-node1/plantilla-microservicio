import { prop, modelOptions, getModelForClass } from '@typegoose/typegoose';
import { IsNotEmpty } from 'class-validator';

@modelOptions({ schemaOptions: { collection: 'vets' } })
export class VeterinaryModel {
  /** Id de mongo */
  _id: string;

  /** Este es el nombre de la veterinaria */
  @IsNotEmpty()
  @prop()
  name: string;

  /** La dirección de la veterinaria */
  @IsNotEmpty()
  @prop()
  address: string;
}


