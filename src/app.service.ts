import RegisterDogDto from "./dto/register-dog.dto";
import { getModelForClass } from "@typegoose/typegoose";
import { VeterinaryModel } from './models/veterinary.model';
import { DogModel } from "./models/dog.model";

export default {
  /**
   * Método que hace el registro del perrito en una veterinaria
   */
  registerDogInVeterinary: async (registerDogDto: RegisterDogDto) => {
    const veterinaryModel = getModelForClass(VeterinaryModel);
    const dogModel = getModelForClass(DogModel);
    // Primero voy a buscar la veterinaria a ver si existe
    const veterinaryFound = await veterinaryModel.findOne({
      _id: registerDogDto.veterinaryId,
    });
    if (!veterinaryFound) {
      throw new Error(`No se encontró la veterinaria con el id: ${registerDogDto.veterinaryId}`)
    }
    const dogCreated = await dogModel.create({
      name: registerDogDto.dog.name,
      color: registerDogDto.dog.color,
    });
    // Aqui asigno la veterinaria al perrito todo minito
    dogCreated.veterinary = veterinaryFound;
    await dogCreated.save();
    return dogCreated;
  }
}