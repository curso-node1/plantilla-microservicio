import {IsNotEmpty, IsIn} from 'class-validator';

class RequestMicroservice {
  /** Nombre del perrito */
  @IsNotEmpty()
  nameDog: string;

  /** Color del perrito */
  @IsIn(['Rojo', 'Verde', 'Azul'], {
    message: 'El color del perrito debe ser Rojo, Verde y Azul'
  })
  colorDog: string;

  /** Id de la veterinaria en la que se quiere registrar el perrito todo precioso */
  @IsNotEmpty()
  veterinaryId: string;
}

export {
  RequestMicroservice,
}