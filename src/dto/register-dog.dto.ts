/**
 * Estos son los datos que necesito para registrar un perrito
 */
interface DogRegister {
  name: string,
  color: string,
}

class RegisterDogDto {
  dog: DogRegister;
  veterinaryId: string
}

export default RegisterDogDto;