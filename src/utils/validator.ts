import { validate } from "class-validator";
import { plainToClass } from "class-transformer";

const validateRequest = async (classType: any, plainObject: Object) => {
  const requestMicroservice = plainToClass(classType, plainObject);
  // Hacer la validación
  const validationErrors = await validate(requestMicroservice);
  if (validationErrors.length > 0) {
    console.log('ValidationErrors: ', validationErrors)
    const errorMessage = Object.values(validationErrors[0].constraints)[0];
    throw new Error(errorMessage);
  }
}

export {
  validateRequest
}