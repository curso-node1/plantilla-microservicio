import { connect, connection, Connection } from 'mongoose';
import { MockMongoose } from 'mock-mongoose';
import * as mongoose from 'mongoose';


export class MongoConnectionTest {
  private mongoURL: string = process.env.MONGO_URL_TEST || '';
  private mockMongoose: MockMongoose;
  public db: Connection;
  
  /**
   * Función que hace la conexión de mongo
   */
  public async connectToMongo() {
    
    // let mongoose = new Mongoose();
    this.mockMongoose = new MockMongoose(mongoose);
    await this.mockMongoose.prepareStorage();
    const MONGO_CONNECTION_OPTIONS = {
      useNewUrlParser: true,
      useUnifiedTopology: true
    };
    connect('mongodb://admin:SuperSecretPassword@localhost:27018/admin', MONGO_CONNECTION_OPTIONS);
    this.db = connection;
    connection.on('connected', () => {
      console.log('db connection is now open');
    });
  };

  public async removeCollectionsFromTemporaryStore() {
    // remove ALL of the collections from a temporary store
    await this.mockMongoose.helper.reset();
  }
}
