import { MongoConnectionTest } from "./database/MongoConnectionTest"
import appServiceTestSetup from './setup';
import appService from "../app.service";
import { DOG_TEST_CASE, VETERINARY_ID_THAT_NOT_EXIST, VETERINARY_ID_TEST_CASE } from "./constants";
import { getModelForClass } from "@typegoose/typegoose";
import { DogModel } from "../models/dog.model";
import { validate } from "class-validator";
import { plainToClass } from "class-transformer";
import { VeterinaryModel } from "../models/veterinary.model";

describe('app.service.ts', () => {
  let mongoConnectionTest: MongoConnectionTest;
  let dogModel = getModelForClass(DogModel);

  beforeAll(async () => {
    mongoConnectionTest = new MongoConnectionTest();
    await mongoConnectionTest.connectToMongo();
  })

  describe('registerDogInVeterinary', () => {
    afterEach(async () => {
      await mongoConnectionTest.removeCollectionsFromTemporaryStore();
      await appServiceTestSetup.setUpVeterinary();
    })
    it('Debe no registrar el perro si no se encontró la veterinaria', async () => {
      try {
        await appService.registerDogInVeterinary({
          dog: {
            name: DOG_TEST_CASE.name,
            color: DOG_TEST_CASE.color,
          },
          veterinaryId: VETERINARY_ID_THAT_NOT_EXIST,
        });
      } catch (error) {
        const dog = await dogModel.find();
        expect(dog.length).toBe(0);
      }
    })
    it('Debe registrar el perro si se encontró la veterinaria', async () => {
      // Registramos el perrito
      await appService.registerDogInVeterinary({
        dog: {
          name: DOG_TEST_CASE.name,
          color: DOG_TEST_CASE.color,
        },
        veterinaryId: VETERINARY_ID_TEST_CASE
      });
      // Buscamos en la base de datos para ver si se guardó
      const dogs = await dogModel.find().lean(); // El lean es para que regrese javascript plano
      const dog = plainToClass(DogModel, dogs[0]);
      const validationErrors = await validate(plainToClass(DogModel, dog));
      expect(validationErrors.length).toBe(0);
    })
  })
})